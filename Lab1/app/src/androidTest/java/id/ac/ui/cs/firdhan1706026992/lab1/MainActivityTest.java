package id.ac.ui.cs.firdhan1706026992.lab1;

import android.widget.Button;
import android.widget.EditText;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void editTextExists() {
        MainActivity ma = new MainActivity();
        EditText et = (EditText) ma.findViewById(R.id.editTextInput);
        assertNotNull(et);
    }

    @Test
    public void buttonExists() {
        MainActivity ma = new MainActivity();
        Button bt = (Button) ma.findViewById(R.id.button);
        assertNotNull(bt);
    }

    @After
    public void tearDown() throws Exception {
    }
}