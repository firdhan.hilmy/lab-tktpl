package id.ac.ui.cs.firdhan1706026992.lab1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editTextInput = (EditText) findViewById(R.id.editTextInput);
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(editTextInput.getText().toString()) &&
                        isPalindrome(editTextInput.getText().toString())) {
                    Toast.makeText(getApplicationContext(), editTextInput.getText().toString() + " Palindrom",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Salah", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean isPalindrome(String word) {
        int i = 0, j = word.length() - 1;
        while (i < j) {

            if (word.charAt(i) != word.charAt(j))
                return false;
            i++;
            j--;
        }

        return true;
    }
}
