package id.ac.ui.cs.firdhan1706026992.lab1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void palindromeFun() {
        String word = "katak";
        boolean result;

        MainActivity mainActivity = new MainActivity();
        result = mainActivity.isPalindrome(word);
        assertTrue(result);
    }

    @After
    public void tearDown() throws Exception {
    }
}