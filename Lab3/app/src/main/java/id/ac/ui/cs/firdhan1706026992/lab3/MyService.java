package id.ac.ui.cs.firdhan1706026992.lab3;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.provider.Settings;

import androidx.annotation.Nullable;

public class MyService extends Service {
    private MediaPlayer sound;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sound = MediaPlayer.create(this, Settings.System.DEFAULT_ALARM_ALERT_URI);
        sound.setLooping(true);
        sound.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sound.stop();
    }
}
